package br.com.snacktime.api.resource;

import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.snacktime.api.event.RecursoCriadoEvent;
import br.com.snacktime.api.model.Responsavel;
import br.com.snacktime.api.repository.ResponsavelRepository;
import br.com.snacktime.api.repository.filter.ResponsavelFilter;
import br.com.snacktime.api.service.ResponsavelService;

@RestController
@RequestMapping("/responsaveis")
public class ResponsavelResource {

	@Autowired
	private ResponsavelRepository responsavelRepository;
	
	@Autowired
	private ResponsavelService responsavelService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	private Page<Responsavel> pesquisar(ResponsavelFilter responsavelFilter, Pageable pageable) {
		return responsavelRepository.filtrar(responsavelFilter, pageable);
	}
	
	@PostMapping
	public ResponseEntity<Responsavel> criar(@Valid @RequestBody Responsavel responsavel, HttpServletResponse response) {
		Responsavel responsavelSalvo = responsavelService.salvar(responsavel);
		
		publisher.publishEvent(new RecursoCriadoEvent(this, response, responsavelSalvo.getId()));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(responsavelSalvo);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Responsavel> buscarPeloId(@PathVariable Long id) {
		Optional<Responsavel> responsavel = responsavelRepository.findById(id);
		return responsavel.isPresent() ? ResponseEntity.ok(responsavel.get()) : ResponseEntity.notFound().build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Responsavel> atualizar(@PathVariable Long id, @Valid @RequestBody Responsavel responsavel) {
		Responsavel responsavelSalvo = responsavelService.atualizar(id, responsavel);
		return ResponseEntity.ok(responsavelSalvo);
	}
	
	/**
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long id) {
		responsavelRepository.deleteById(id);
	}
	**/
}
