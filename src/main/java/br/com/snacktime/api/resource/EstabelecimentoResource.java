package br.com.snacktime.api.resource;

import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.snacktime.api.event.RecursoCriadoEvent;
import br.com.snacktime.api.model.Estabelecimento;
import br.com.snacktime.api.repository.EstabelecimentoRepository;
import br.com.snacktime.api.repository.filter.EstabelecimentoFilter;
import br.com.snacktime.api.service.EstabelecimentoService;

@RestController
@RequestMapping("/estabelecimentos")
public class EstabelecimentoResource {

	@Autowired
	private EstabelecimentoRepository estabelecimentoRepository;
	
	@Autowired
	private EstabelecimentoService estabelecimentoService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	private Page<Estabelecimento> pesquisar(EstabelecimentoFilter estabelecimentoFilter, Pageable pageable) {
		return estabelecimentoRepository.filtrar(estabelecimentoFilter, pageable);
	}
	
	@PostMapping
	public ResponseEntity<Estabelecimento> criar(@RequestBody @Valid Estabelecimento estabelecimento, HttpServletResponse response) {
		Estabelecimento estabelecimentoSalvo = estabelecimentoService.salvar(estabelecimento);
		
		publisher.publishEvent(new RecursoCriadoEvent(this, response, estabelecimentoSalvo.getId()));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(estabelecimentoSalvo);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Estabelecimento> buscarPeloId(@PathVariable Long id) {
		Optional<Estabelecimento> estabelecimento = estabelecimentoRepository.findById(id);
		return estabelecimento.isPresent() ? ResponseEntity.ok(estabelecimento.get()) : ResponseEntity.notFound().build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Estabelecimento> atualizar(@PathVariable Long id, @Valid @RequestBody Estabelecimento estabelecimento) {
		Estabelecimento estabelecimentoSalvo = estabelecimentoService.atualizar(id, estabelecimento);
		return ResponseEntity.ok(estabelecimentoSalvo);
	}
	
	@PutMapping("/{id}/ativo")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void atualizarPropriedadeAtivo(@PathVariable Long id, @RequestBody Boolean ativo) {
		estabelecimentoService.atualizarPropriedadeAtivo(id, ativo);
	}
	
}
