package br.com.snacktime.api.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.snacktime.api.event.RecursoCriadoEvent;
import br.com.snacktime.api.model.Vinculado;
import br.com.snacktime.api.repository.VinculadoRepository;
import br.com.snacktime.api.repository.filter.VinculadoFilter;
import br.com.snacktime.api.repository.projection.ResumoVinculado;
import br.com.snacktime.api.service.VinculadoService;

@RestController
@RequestMapping("/vinculados")
public class VinculadoResource {

	@Autowired
	private VinculadoRepository vinculadoRepository;
	
	@Autowired
	private VinculadoService vinculadoService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	public Page<Vinculado> filtrar(VinculadoFilter vinculadoFilter, Pageable pageable) {
		return vinculadoRepository.filtrar(vinculadoFilter, pageable);
	}
	
	@GetMapping(params = "resumo")
	public List<ResumoVinculado> resumir(VinculadoFilter vinculadoFilter) {
		return vinculadoRepository.resumir(vinculadoFilter);
	}
	
	@PostMapping
	public ResponseEntity<Vinculado> criar(@RequestBody @Valid Vinculado vinculado, HttpServletResponse response) {
		Vinculado vinculadoSalvo = vinculadoService.salvar(vinculado);
		
		publisher.publishEvent(new RecursoCriadoEvent(this, response, vinculadoSalvo.getId()));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(vinculadoSalvo);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Vinculado> buscarPeloId(@PathVariable Long id) {
		Vinculado vinculado = vinculadoService.buscarPeloId(id);
		return vinculado != null ? ResponseEntity.ok(vinculado) : ResponseEntity.notFound().build();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Vinculado> atualizar(@PathVariable Long id, @Valid @RequestBody Vinculado vinculado) {
		Vinculado vinculadoSalvo = vinculadoService.atualizar(id, vinculado);
		return ResponseEntity.ok(vinculadoSalvo);
	}
	
	/**
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long id) {
		vinculadoRepository.deleteById(id);
	}
	**/
}
