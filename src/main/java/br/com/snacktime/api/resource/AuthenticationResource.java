package br.com.snacktime.api.resource;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import br.com.snacktime.api.model.Usuario;
import br.com.snacktime.api.repository.UsuarioRepository;
import lombok.Getter;
import lombok.Setter;

@PropertySource("classpath:fb.properties")
@CrossOrigin("*")
@RestController
public class AuthenticationResource {

	@Value("${fb.app.id}")
    private String fbAppId;
	
	@Value("${fb.ak.secret}")
    private String  fbAkSecret;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	private static final String ME_ENDPOINT_BASE_URL = "https://graph.accountkit.com/v1.1/me";
    private static final String TOKEN_EXCHANGE_BASE_URL = "https://graph.accountkit.com/v1.1/access_token";
	
	@PostMapping("/smslogin")
	public ResponseEntity<?> smsLogin(@RequestBody String authorizationCode) throws Exception {
		
		String uri = TOKEN_EXCHANGE_BASE_URL + "?grant_type=authorization_code&code="
				+ authorizationCode.replaceAll("\"", "") + "&access_token=AA|"
				+ fbAppId + "|" + fbAkSecret;
		AccountKitPostResponse accountKitPostResponse  = restTemplate.getForObject(uri, AccountKitPostResponse.class);
		
		
		String meEndPointUri = ME_ENDPOINT_BASE_URL + "?access_token="
				+ accountKitPostResponse.getAccess_token();
		PersonInfo personInfo = restTemplate.getForObject(meEndPointUri, PersonInfo.class);
		
		Optional<Usuario> usuarioExistente = usuarioRepository.findByTelefone(personInfo.getPhone().getNational_number());		
		if(!usuarioExistente.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		
		return getToken(usuarioExistente.get(), "mobile");
	}

	private ResponseEntity<?> getToken(Usuario usuario, String client) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.setBasicAuth("angular", "@ngul@r0");
		
		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("client", client);
		map.add("username", usuario.getTelefone());
		map.add("password", usuario.getTelefone());
		map.add("grant_type", "password");
		
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		ResponseEntity<?> response = restTemplate.postForEntity( "http://localhost:8080/oauth/token", request , Object.class );
		
		return ResponseEntity.ok(response);
	}
	
	@Getter @Setter
	static class AccountKitPostResponse {
		private String id;
		private String access_token;
		private String token_refresh_interval_sec;
		
	}
	
	@Getter @Setter
	static class PersonInfo {
		private String id;
		private Phone phone;
		private Application application;
	}
	
	@Getter @Setter
	static class Phone {
		private String number;
		private String country_prefix;
		private String national_number;
	}
	
	@Getter @Setter
	static class Application {
		private String id;
	}

}
