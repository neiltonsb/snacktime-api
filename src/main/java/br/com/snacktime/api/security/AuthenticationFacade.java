package br.com.snacktime.api.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Component;

import br.com.snacktime.api.model.Responsavel;
import br.com.snacktime.api.model.Usuario;
import br.com.snacktime.api.repository.ResponsavelRepository;
import br.com.snacktime.api.repository.UsuarioRepository;

@Component
public class AuthenticationFacade implements IAuthenticationFacade {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private ResponsavelRepository responsavelRepository;
	
	@Override
	public Usuario getUsuarioLogado() {
		OAuth2Authentication auth = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
		Usuario usuarioLogado = usuarioRepository.findByTelefone(auth.getName()).get();
		
		return usuarioLogado;
	}

	@Override
	public Responsavel getResponsavelLogado() {
		Responsavel responsavelLogado = responsavelRepository.findByUsuario(getUsuarioLogado()).get();
		return responsavelLogado;
	}

}
