package br.com.snacktime.api.security;

import br.com.snacktime.api.model.Responsavel;
import br.com.snacktime.api.model.Usuario;

public interface IAuthenticationFacade {

	Usuario getUsuarioLogado();
	
	Responsavel getResponsavelLogado();
}
