package br.com.snacktime.api.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.snacktime.api.model.Cartao;
import br.com.snacktime.api.repository.CartaoRepository;

@Service
public class CartaoService {

	@Autowired
	private CartaoRepository cartaoRepository;
	
	public void atualizarPropriedadeAtivo(Long id, Boolean ativo) {
		Cartao cartaoSalvo = buscarCartaoPeloId(id);
		cartaoSalvo.setAtivo(ativo);
		
		cartaoRepository.save(cartaoSalvo);
	}

	private Cartao buscarCartaoPeloId(Long id) {
		Optional<Cartao> cartaoSalvo = cartaoRepository.findById(id);
		if (!cartaoSalvo.isPresent()) {
			throw new EmptyResultDataAccessException(1);
		}
		return cartaoSalvo.get();
	}

	
	
}
