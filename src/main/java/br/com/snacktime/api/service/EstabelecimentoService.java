package br.com.snacktime.api.service;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.snacktime.api.model.Estabelecimento;
import br.com.snacktime.api.repository.EstabelecimentoRepository;
import br.com.snacktime.api.service.exception.CpfCnpjJaCadastradoException;

@Service
public class EstabelecimentoService {

	@Autowired
	private EstabelecimentoRepository estabelecimentoRepository;
	
	public Estabelecimento salvar(@Valid Estabelecimento estabelecimento) {
		Optional<Estabelecimento> estabelecimentoExistente = estabelecimentoRepository.findByCpfCnpj(estabelecimento.getCpfCnpj());
		
		if(estabelecimentoExistente.isPresent() && !estabelecimentoExistente.get().equals(estabelecimento)) {
			throw new CpfCnpjJaCadastradoException();
		}
		
		return estabelecimentoRepository.save(estabelecimento);
	}

	public Estabelecimento atualizar(Long id, Estabelecimento estabelecimento) {
		Estabelecimento estabelecimentoSalvo = buscarEstabelecimentoPeloId(id);
		
		Optional<Estabelecimento> estabelecimentoExistente = estabelecimentoRepository.findByCpfCnpj(estabelecimento.getCpfCnpj());
		if(estabelecimentoExistente.isPresent() && !estabelecimentoExistente.get().equals(estabelecimentoSalvo)) {
			throw new CpfCnpjJaCadastradoException();
		}
		
		BeanUtils.copyProperties(estabelecimento, estabelecimentoSalvo, "id");
		
		return estabelecimentoRepository.save(estabelecimentoSalvo);
	}
	
	public void atualizarPropriedadeAtivo(Long id, Boolean ativo) {
		Estabelecimento estabelecimentoSalvo = buscarEstabelecimentoPeloId(id);
		estabelecimentoSalvo.setAtivo(ativo);
		
		estabelecimentoRepository.save(estabelecimentoSalvo);
	}

	private Estabelecimento buscarEstabelecimentoPeloId(Long id) {
		Optional<Estabelecimento> estabelecimentoSalvo = estabelecimentoRepository.findById(id);
		if (!estabelecimentoSalvo.isPresent()) {
			throw new EmptyResultDataAccessException(1);
		}
		return estabelecimentoSalvo.get();
	}

}
