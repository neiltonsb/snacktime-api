package br.com.snacktime.api.service;

import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.snacktime.api.model.Cartao;
import br.com.snacktime.api.model.Responsavel;
import br.com.snacktime.api.model.Usuario;
import br.com.snacktime.api.model.Vinculado;
import br.com.snacktime.api.repository.CartaoRepository;
import br.com.snacktime.api.repository.VinculadoRepository;
import br.com.snacktime.api.security.IAuthenticationFacade;

@Service
public class VinculadoService {
	
	@Autowired
	private VinculadoRepository vinculadoRepository;
	
	@Autowired
	private CartaoRepository cartaoRepository;
	
	@Autowired
	private IAuthenticationFacade authenticationFacade;

	public Vinculado salvar(Vinculado vinculado) {
		vinculado.setCartao(gerarCartao(vinculado));
		
		return vinculadoRepository.save(vinculado);
	}
	
	public Vinculado atualizar(Long id, @Valid Vinculado vinculado) {
		Vinculado vinculadoSalvo = buscarPeloId(id);
		
		BeanUtils.copyProperties(vinculado, vinculadoSalvo, "id", "cartao", "responsavel");
		
		return vinculadoRepository.save(vinculadoSalvo);
	}
	
	private Cartao gerarCartao(Vinculado vinculado) {
		String codigo = UUID.randomUUID().toString().substring(0, 4).toUpperCase()
				.concat(UUID.randomUUID().toString().substring(0, 4).toUpperCase())
				.concat(UUID.randomUUID().toString().substring(0, 4).toUpperCase());
		
		Optional<Cartao> cartaoExistente = cartaoRepository.findByCodigoIgnoreCase(codigo);
		
		if(cartaoExistente.isPresent()) {
			return gerarCartao(vinculado);
		}
		
		Cartao cartao = new Cartao();
		cartao.setCodigo(codigo);
		cartao.setIlimitado(false);
		cartao.setAtivo(true);
		cartao.setVinculado(vinculado);
		
		return cartao;
	}
	
	public Vinculado buscarPeloId(Long id) {
		Optional<Vinculado> vinculadoSalvo = vinculadoRepository.findById(id);
		if (!vinculadoSalvo.isPresent()) {
			throw new EmptyResultDataAccessException(1);
		}
		
		Usuario usuarioLogado = authenticationFacade.getUsuarioLogado();
		if(usuarioLogado.getTipoUsuario().equals("RESPONSAVEL")) {
			Responsavel responsavelLogado = authenticationFacade.getResponsavelLogado();
			if(!vinculadoSalvo.get().getResponsavel().equals(responsavelLogado)) {
				throw new ResponseStatusException(HttpStatus.FORBIDDEN);
			}
		}
		
		
		return vinculadoSalvo.get();
	}
	
}
