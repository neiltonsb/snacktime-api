package br.com.snacktime.api.service;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.snacktime.api.model.Cartao;
import br.com.snacktime.api.model.Responsavel;
import br.com.snacktime.api.model.Usuario;
import br.com.snacktime.api.repository.CartaoRepository;
import br.com.snacktime.api.repository.PermissaoRepository;
import br.com.snacktime.api.repository.ResponsavelRepository;
import br.com.snacktime.api.service.exception.EmailJaCadastradoException;

@Service
public class ResponsavelService {

	@Autowired
	private ResponsavelRepository responsavelRepository;
	
	@Autowired
	private CartaoRepository cartaoRepository;
	
	@Autowired
	private PermissaoRepository permissaoRepository;
	
	@Autowired
	private PasswordEncoder encoder;
	
	
	public Responsavel salvar(Responsavel responsavel) {
		Optional<Responsavel> responsavelExistente = responsavelRepository.findByEmailIgnoreCase(responsavel.getEmail());
		
		if(responsavelExistente.isPresent() && !responsavelExistente.get().equals(responsavel)) {
			throw new EmailJaCadastradoException();
		}
		
	
		responsavel.setCartao(gerarCartao(responsavel));
		responsavel.setUsuario(gerarUsuario(responsavel));
		
		return responsavelRepository.save(responsavel);
	}

	private Usuario gerarUsuario(Responsavel responsavel) {
		Usuario usuario = new Usuario();
		usuario.setNome(responsavel.getNome());
		usuario.setTipoUsuario("RESPONSAVEL");
		usuario.setTelefone(responsavel.getTelefone().replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("-", "").replaceAll(" ", ""));
		usuario.setSenha(encoder.encode(usuario.getTelefone()));
		usuario.setPermissoes(permissaoRepository.findByDescricao("ROLE_CADASTRAR_VINCULADO"));
		
		return usuario;
	}

	private Cartao gerarCartao(Responsavel responsavel) {
		String codigo = UUID.randomUUID().toString().substring(0, 4).toUpperCase()
				.concat(UUID.randomUUID().toString().substring(0, 4).toUpperCase())
				.concat(UUID.randomUUID().toString().substring(0, 4).toUpperCase());
		
		Optional<Cartao> cartaoExistente = cartaoRepository.findByCodigoIgnoreCase(codigo);
		
		if(cartaoExistente.isPresent()) {
			return gerarCartao(responsavel);
		}
		
		Cartao cartao = new Cartao();
		cartao.setCodigo(codigo);
		cartao.setIlimitado(true);
		cartao.setAtivo(true);
		cartao.setResponsavel(responsavel);
		
		return cartao;
	}

	public Responsavel atualizar(Long id, Responsavel responsavel) {
		Responsavel responsavelSalvo = buscarPeloId(id);
		
		Optional<Responsavel> responsavelExistente = responsavelRepository.findByEmailIgnoreCase(responsavel.getEmail());
		if(responsavelExistente.isPresent() && !responsavelExistente.get().equals(responsavelSalvo)) {
			throw new EmailJaCadastradoException();
		}
		
		BeanUtils.copyProperties(responsavel, responsavelSalvo, "id", "cartao", "vinculados");
		
		return responsavelRepository.save(responsavelSalvo);
	}
	
	private Responsavel buscarPeloId(Long id) {
		Optional<Responsavel> responsavelSalvo = responsavelRepository.findById(id);
		if (!responsavelSalvo.isPresent()) {
			throw new EmptyResultDataAccessException(1);
		}
		return responsavelSalvo.get();
	}

}
