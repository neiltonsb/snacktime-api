package br.com.snacktime.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.snacktime.api.model.Estabelecimento;
import br.com.snacktime.api.repository.estabelecimento.EstabelecimentoRepositoryQuery;

public interface EstabelecimentoRepository extends JpaRepository<Estabelecimento, Long>, EstabelecimentoRepositoryQuery{

	Optional<Estabelecimento> findByCpfCnpj(String cpfCnpj);

}
