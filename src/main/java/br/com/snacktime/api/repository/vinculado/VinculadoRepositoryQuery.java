package br.com.snacktime.api.repository.vinculado;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.snacktime.api.model.Vinculado;
import br.com.snacktime.api.repository.filter.VinculadoFilter;
import br.com.snacktime.api.repository.projection.ResumoVinculado;

public interface VinculadoRepositoryQuery {

	public Page<Vinculado> filtrar(VinculadoFilter vinculadoFilter, Pageable pageable);
	
	public List<ResumoVinculado> resumir(VinculadoFilter vinculadoFilter);	
}
