package br.com.snacktime.api.repository.responsavel;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import br.com.snacktime.api.model.Responsavel_;
import br.com.snacktime.api.model.Responsavel;
import br.com.snacktime.api.repository.filter.ResponsavelFilter;
import br.com.snacktime.api.repository.paginacao.PaginacaoUtil;

public class ResponsavelRepositoryImpl implements ResponsavelRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@Override
	public Page<Responsavel> filtrar(ResponsavelFilter responsavelFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Responsavel> criteria = builder.createQuery(Responsavel.class);
		Root<Responsavel> root = criteria.from(Responsavel.class);
		
		Predicate[] predicates = criarRestricoes(responsavelFilter, builder, root);
		criteria.where(predicates);
		
		paginacaoUtil.adicionarRestricoesDeOrdenacao(builder, root, criteria, pageable);
		TypedQuery<Responsavel> query = manager.createQuery(criteria);
		paginacaoUtil.adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(responsavelFilter));
	}

	private Long total(ResponsavelFilter responsavelFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Responsavel> root = criteria.from(Responsavel.class);
		
		Predicate[] predicates = criarRestricoes(responsavelFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	private Predicate[] criarRestricoes(ResponsavelFilter responsavelFilter, CriteriaBuilder builder,
			Root<Responsavel> root) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		if(!StringUtils.isEmpty(responsavelFilter.getNome())) {
			predicates.add(
						builder.like(builder.lower(root.get(Responsavel_.nome)), "%" + responsavelFilter.getNome() + "%")
					);
		}
		
		if(!StringUtils.isEmpty(responsavelFilter.getEmail())) {
			predicates.add(
						builder.like(builder.lower(root.get(Responsavel_.email)), "%" + responsavelFilter.getEmail() + "%")
					);
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}

}
