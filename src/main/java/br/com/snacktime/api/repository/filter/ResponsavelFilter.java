package br.com.snacktime.api.repository.filter;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ResponsavelFilter {

	private String nome;
	private String email;
	private Boolean ativo;
	
}
