package br.com.snacktime.api.repository.estabelecimento;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.snacktime.api.model.Estabelecimento;
import br.com.snacktime.api.repository.filter.EstabelecimentoFilter;

public interface EstabelecimentoRepositoryQuery {

	public Page<Estabelecimento> filtrar(EstabelecimentoFilter estabelecimentoFilter, Pageable pageable);
	
}
