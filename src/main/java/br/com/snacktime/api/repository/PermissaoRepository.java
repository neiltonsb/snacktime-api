package br.com.snacktime.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.snacktime.api.model.Permissao;

public interface PermissaoRepository extends JpaRepository<Permissao, Long>{

	List<Permissao> findByDescricao(String string);

}
