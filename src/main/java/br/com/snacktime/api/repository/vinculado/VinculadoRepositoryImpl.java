package br.com.snacktime.api.repository.vinculado;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import br.com.snacktime.api.model.Cartao;
import br.com.snacktime.api.model.Responsavel;
import br.com.snacktime.api.model.Usuario;
import br.com.snacktime.api.model.Vinculado;
import br.com.snacktime.api.model.Vinculado_;
import br.com.snacktime.api.repository.ResponsavelRepository;
import br.com.snacktime.api.repository.UsuarioRepository;
import br.com.snacktime.api.repository.filter.VinculadoFilter;
import br.com.snacktime.api.repository.paginacao.PaginacaoUtil;
import br.com.snacktime.api.repository.projection.ResumoVinculado;

public class VinculadoRepositoryImpl implements VinculadoRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private ResponsavelRepository responsavelRepository;
	
	@Override
	public Page<Vinculado> filtrar(VinculadoFilter vinculadoFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Vinculado> criteria = builder.createQuery(Vinculado.class);
		Root<Vinculado> root = criteria.from(Vinculado.class);
		
		Predicate[] predicates = criarRestricoes(vinculadoFilter, builder, root);
		criteria.where(predicates);
		
		paginacaoUtil.adicionarRestricoesDeOrdenacao(builder, root, criteria, pageable);
		TypedQuery<Vinculado> query = manager.createQuery(criteria);
		paginacaoUtil.adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(vinculadoFilter));
	}
	
	@Override
	public List<ResumoVinculado> resumir(VinculadoFilter vinculadoFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoVinculado> criteria = builder.createQuery(ResumoVinculado.class);
		Root<Vinculado> root = criteria.from(Vinculado.class);
		
		criteria.select(builder.construct(ResumoVinculado.class, 
				root.get(Vinculado_.ID),
				root.get(Vinculado_.NOME),
				root.get(Vinculado_.TELEFONE)));
		
		Predicate[] predicates = criarRestricoes(vinculadoFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<ResumoVinculado> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
	private Long total(VinculadoFilter vinculadoFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Vinculado> root = criteria.from(Vinculado.class);
		
		Predicate[] predicates = criarRestricoes(vinculadoFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
	
	private Predicate[] criarRestricoes(VinculadoFilter vinculadoFilter, CriteriaBuilder builder,
			Root<Vinculado> root) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		if(!StringUtils.isEmpty(vinculadoFilter.getNome())) {
			predicates.add(
						builder.like(builder.lower(root.get(Vinculado_.nome)), "%" + vinculadoFilter.getNome() + "%")
					);
		}
		
		
		OAuth2Authentication auth = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
		Usuario usuarioLogado = usuarioRepository.findByTelefone(auth.getName()).get();
		
		if(usuarioLogado.getTipoUsuario().equals("RESPONSAVEL")) {
			Responsavel responsavelUsuario = responsavelRepository.findByUsuario(usuarioLogado).get();
			
			predicates.add(
						builder.equal(root.get(Vinculado_.RESPONSAVEL), responsavelUsuario)
					);
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}


}
