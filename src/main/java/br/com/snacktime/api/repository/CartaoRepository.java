package br.com.snacktime.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.snacktime.api.model.Cartao;

public interface CartaoRepository extends JpaRepository<Cartao, Long>{

	Optional<Cartao> findByCodigoIgnoreCase(String codigo);

}
