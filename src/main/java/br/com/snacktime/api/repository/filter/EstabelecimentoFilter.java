package br.com.snacktime.api.repository.filter;

import br.com.snacktime.api.model.TipoPessoa;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class EstabelecimentoFilter {

	private String cpfCnpj;
	private String nomeRazaoSocial;
	private String cidade;
	private String estado;
	private TipoPessoa tipoPessoa;
	private Boolean ativo;
	
}
