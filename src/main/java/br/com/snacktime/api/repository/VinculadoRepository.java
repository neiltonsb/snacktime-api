package br.com.snacktime.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.snacktime.api.model.Vinculado;
import br.com.snacktime.api.repository.vinculado.VinculadoRepositoryQuery;

public interface VinculadoRepository extends JpaRepository<Vinculado, Long>, VinculadoRepositoryQuery {

}
