package br.com.snacktime.api.repository.projection;

import br.com.snacktime.api.model.Cartao;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ResumoVinculado {

	
	private Long id;
	private String nome;
	private String telefone;
	private Cartao cartao;
	
	public ResumoVinculado(Long id, String nome, String telefone, Cartao cartao) {
		this.id = id;
		this.nome = nome;
		this.telefone = telefone;
		this.cartao = cartao;
	}
	
	
}
