package br.com.snacktime.api.repository.estabelecimento;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import br.com.snacktime.api.model.Estabelecimento_;
import br.com.snacktime.api.model.Estabelecimento;
import br.com.snacktime.api.repository.filter.EstabelecimentoFilter;
import br.com.snacktime.api.repository.paginacao.PaginacaoUtil;

public class EstabelecimentoRepositoryImpl implements EstabelecimentoRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Autowired
	private PaginacaoUtil paginacaoUtil;
	
	@Override
	public Page<Estabelecimento> filtrar(EstabelecimentoFilter estabelecimentoFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Estabelecimento> criteria = builder.createQuery(Estabelecimento.class);
		Root<Estabelecimento> root = criteria.from(Estabelecimento.class);
		
		Predicate[] predicates = criarRestricoes(estabelecimentoFilter, builder, root);
		criteria.where(predicates);
		
		paginacaoUtil.adicionarRestricoesDeOrdenacao(builder, root, criteria, pageable);
		TypedQuery<Estabelecimento> query = manager.createQuery(criteria);
		paginacaoUtil.adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(estabelecimentoFilter));
	}
	
	private Long total(EstabelecimentoFilter estabelecimentoFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Estabelecimento> root = criteria.from(Estabelecimento.class);
		
		Predicate[] predicates = criarRestricoes(estabelecimentoFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	private Predicate[] criarRestricoes(EstabelecimentoFilter estabelecimentoFilter, CriteriaBuilder builder,
			Root<Estabelecimento> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if(estabelecimentoFilter.getTipoPessoa() != null) {
			predicates.add(builder.equal(root.get(Estabelecimento_.tipoPessoa), estabelecimentoFilter.getTipoPessoa()));
		}
		
		if(!StringUtils.isEmpty(estabelecimentoFilter.getCpfCnpj())) {
			predicates.add(builder.like(root.get(Estabelecimento_.cpfCnpj), "%" + estabelecimentoFilter.getCpfCnpj() + "%"));
		}
		
		if(!StringUtils.isEmpty(estabelecimentoFilter.getNomeRazaoSocial())) {
			predicates.add(builder.like(root.get(Estabelecimento_.nomeRazaoSocial), "%" + estabelecimentoFilter.getNomeRazaoSocial() + "%"));
		}
		
		if(!StringUtils.isEmpty(estabelecimentoFilter.getCidade())) {
			predicates.add(builder.like(root.get(Estabelecimento_.cidade), "%" + estabelecimentoFilter.getCidade() + "%"));
		}
		
		if(!StringUtils.isEmpty(estabelecimentoFilter.getEstado())) {
			predicates.add(builder.like(root.get(Estabelecimento_.estado), "%" + estabelecimentoFilter.getEstado() + "%"));
		}
		
		if(estabelecimentoFilter.getAtivo() != null) {
			predicates.add(builder.equal(root.get(Estabelecimento_.ativo), estabelecimentoFilter.getAtivo()));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}


}
