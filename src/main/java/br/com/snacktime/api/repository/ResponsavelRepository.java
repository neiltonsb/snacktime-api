package br.com.snacktime.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.snacktime.api.model.Responsavel;
import br.com.snacktime.api.model.Usuario;
import br.com.snacktime.api.repository.responsavel.ResponsavelRepositoryQuery;

public interface ResponsavelRepository extends JpaRepository<Responsavel, Long>, ResponsavelRepositoryQuery{

	Optional<Responsavel> findByEmailIgnoreCase(String email);

	Optional<Responsavel> findByUsuario(Usuario usuarioLogado);

}
