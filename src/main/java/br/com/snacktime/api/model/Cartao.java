package br.com.snacktime.api.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @EqualsAndHashCode(of = "id")
@Entity
@Table(name = "cartao")
public class Cartao {

	@Id
	@GeneratedValue(strategy  = GenerationType.IDENTITY)
	private Long id;
	
	private String codigo;
	
	private Boolean ilimitado;
	
	@Column(name = "limite_diario")
	private BigDecimal limiteDiario;
	
	private Boolean ativo;
	
	@OneToOne
	@JoinColumn(name = "responsavel_id")
	private Responsavel responsavel;
	
	@OneToOne
	@JoinColumn(name = "vinculado_id")
	private Vinculado vinculado;
	
}
