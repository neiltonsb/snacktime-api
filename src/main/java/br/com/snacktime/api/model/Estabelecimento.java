package br.com.snacktime.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;
import org.hibernate.validator.group.GroupSequenceProvider;

import br.com.snacktime.api.model.validation.EstabelecimentoGroupSequenceProvider;
import br.com.snacktime.api.model.validation.group.CnpjGroup;
import br.com.snacktime.api.model.validation.group.CpfGroup;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @EqualsAndHashCode(of = "id")
@Entity
@Table(name =  "estabelecimento")
@GroupSequenceProvider(EstabelecimentoGroupSequenceProvider.class)
public class Estabelecimento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_pessoa")
	private TipoPessoa tipoPessoa;
	
	@NotBlank
	@Size(max = 50)
	@Column(name = "nome_razao_social")
	private String nomeRazaoSocial;
	
	@NotBlank
	@CPF(groups = CpfGroup.class)
	@CNPJ(groups = CnpjGroup.class)
	@Column(name = "cpf_cnpj")
	private String cpfCnpj;
	
	@Size(max = 30)
	@Column(name = "inscricao_estadual")
	private String inscricaoEstadual;
	
	@NotBlank
	@Size(max = 15)
	private String telefone;
	
	@Email
	@NotBlank
	@Size(max = 50)
	private String email;
	
	private Boolean ativo = true;
	
	@NotNull
	@Size(max = 50)
	private String logradouro;
	
	@NotNull
	private Integer numero;
	
	@Size(max = 30)
	private String complemento;
	
	@NotBlank
	@Size(max = 50)
	private String bairro;
	
	@NotBlank
	@Size(max = 50)
	private String cidade;
	
	@NotBlank
	@Size(max = 2)
	private String estado;
	
	@NotBlank
	@Size(max = 9)
	private String cep;
}
