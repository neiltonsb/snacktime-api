package br.com.snacktime.api.model.validation;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.validator.spi.group.DefaultGroupSequenceProvider;

import br.com.snacktime.api.model.Estabelecimento;

public class EstabelecimentoGroupSequenceProvider implements DefaultGroupSequenceProvider<Estabelecimento> {

	@Override
	public List<Class<?>> getValidationGroups(Estabelecimento estabelecimento) {
		List<Class<?>> grupos = new ArrayList<>();
		grupos.add(Estabelecimento.class);
		
		if(isTipoPessoaSelecionada(estabelecimento)) {
			grupos.add(estabelecimento.getTipoPessoa().getGrupo());
		}
		
		return grupos;
	}

	private boolean isTipoPessoaSelecionada(Estabelecimento estabelecimento) {
		return estabelecimento != null && estabelecimento.getTipoPessoa() != null;
	}

}
