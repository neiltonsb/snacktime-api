package br.com.snacktime.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AccountKitPostRequest {

	private String code;
	private String csrf;
	
}
