CREATE TABLE usuario (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(50) NOT NULL,
	telefone VARCHAR(15) NOT NULL,
	senha VARCHAR(150) NOT NULL,
    tipo_usuario VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE permissao (
	id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	descricao VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE usuario_permissao (
	usuario_id BIGINT(20) NOT NULL,
	permissao_id BIGINT(20) NOT NULL,
	PRIMARY KEY (usuario_id, permissao_id),
	FOREIGN KEY (usuario_id) REFERENCES usuario(id),
	FOREIGN KEY (permissao_id) REFERENCES permissao(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* Usuário dos Responsáveis */
INSERT INTO usuario (nome, telefone, senha, tipo_usuario) values ('João da Silva', '69989962827', '$2a$10$6LhPbhxBhNxJ4uqjoK/cVuib6ZF3e/oo2c8al8RIv7azvy3VU5Phm', 'RESPONSAVEL');
INSERT INTO usuario (nome, telefone, senha, tipo_usuario) values ('Maria da Costa', '69997665095', '$2a$10$LHCO5c4iMwmq6MkxzqHafOAYN9heGpstvEtYacHiPCeex0VE9Ap12', 'RESPONSAVEL');
INSERT INTO usuario (nome, telefone, senha, tipo_usuario) values ('Joaquim Ribeiro', '69983968091', '$2a$10$1nchAXjwuko3I0PgFIkFyurJUUKoTQ/j6/xGf62pbZnhVoUWw4FrS', 'RESPONSAVEL');
INSERT INTO usuario (nome, telefone, senha, tipo_usuario) values ('Astrogilda Maranhão', '69991356293', '$2a$10$leJJ7uW.dDZw8SRqO6DpOeo.x/uST0jzQ/SeyjodAkfzq7WdZnpBq', 'RESPONSAVEL');
/* end */

INSERT INTO usuario (nome, telefone, senha, tipo_usuario) values ('Paulo da Silva', '69993214366', '$2a$10$zAnqTlhPGZ8rTjjXiBhxheXGtEzX1j6H9T17Hd8pvq9ckEvyvmbRq', 'VINCULADO');
INSERT INTO usuario (nome, telefone, senha, tipo_usuario) values ('Pamela da Silva', '69993629397', '$2a$10$TSocLsjgVJ8O8qzggi7dJuIHkQB5kcm3y53uT76DzrEt3Tk2e2VU2', 'VINCULADO');

INSERT INTO usuario (nome, telefone, senha, tipo_usuario) values ('Marlinda da Costa', '69996964721', '$2a$10$xaIK7NqqEA.k4rDbuojQy.MkclbJ9Uj6fyWphEUcCGoHzTiAaIVAa', 'VINCULADO');
INSERT INTO usuario (nome, telefone, senha, tipo_usuario) values ('Mario da Costa', '69983386594', '$2a$10$Hs5ci47MbugcFPaPN8wuL.4U/uKn5zHBVwqn1rG/L.Ox3E7GPXlNC', 'VINCULADO');

INSERT INTO usuario (nome, telefone, senha, tipo_usuario) values ('Jairo Ribeiro', '69995236890', '$2a$10$MbOh4kqD3f1tAUuUKKYim.NyqGddxRAKKtKhFJg.pXrAVO8qRmeE2', 'VINCULADO');
INSERT INTO usuario (nome, telefone, senha, tipo_usuario) values ('Jaqueline Ribeiro', '69993073651', '$2a$10$/iOsf5RZ.7wMIjQs.NzUjOskiwRPvGrM8fl9K7L7zhbN5sjnRiXvG', 'VINCULADO');

INSERT INTO usuario (nome, telefone, senha, tipo_usuario) values ('Amador Maranhão', '69988347348', '$2a$10$JDegVfaqgPTvfuaHy2MZSetGHty3LVLWzbqHf9ascwVG0P403hibW', 'VINCULADO');
INSERT INTO usuario (nome, telefone, senha, tipo_usuario) values ('Amanda Maranhão', '69991385685', '$2a$10$8hqMGIF9plS/cnBskc.tveWyLfh92I1MzpgL2h89T8WjziWHTMgtW', 'VINCULADO');


INSERT INTO permissao (descricao) values ('ROLE_CADASTRAR_VINCULADO');

INSERT INTO usuario_permissao (usuario_id, permissao_id) values ((SELECT id FROM usuario WHERE nome = 'João da Silva'), (SELECT id FROM permissao WHERE descricao = 'ROLE_CADASTRAR_VINCULADO'));
INSERT INTO usuario_permissao (usuario_id, permissao_id) values ((SELECT id FROM usuario WHERE nome = 'Maria da Costa'), (SELECT id FROM permissao WHERE descricao = 'ROLE_CADASTRAR_VINCULADO'));
INSERT INTO usuario_permissao (usuario_id, permissao_id) values ((SELECT id FROM usuario WHERE nome = 'Joaquim Ribeiro'), (SELECT id FROM permissao WHERE descricao = 'ROLE_CADASTRAR_VINCULADO'));
INSERT INTO usuario_permissao (usuario_id, permissao_id) values ((SELECT id FROM usuario WHERE nome = 'Astrogilda Maranhão'), (SELECT id FROM permissao WHERE descricao = 'ROLE_CADASTRAR_VINCULADO'));
