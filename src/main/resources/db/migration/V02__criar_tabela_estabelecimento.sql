CREATE TABLE estabelecimento (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    tipo_pessoa VARCHAR(8) NOT NULL,
    nome_razao_social VARCHAR(50) NOT NULL,
    nome_fantasia VARCHAR(50),
    cpf_cnpj VARCHAR(18) NOT NULL UNIQUE,
    inscricao_estadual VARCHAR(30),
    telefone VARCHAR(15) NOT NULL,
    email VARCHAR(50) NOT NULL,
    ativo BOOLEAN DEFAULT 1,
    logradouro VARCHAR(50) NOT NULL,
    numero INTEGER NOT NULL,
    complemento VARCHAR(30),
    bairro VARCHAR(50) NOT NULL,
    cidade VARCHAR(50) NOT NULL,
    estado VARCHAR(2) NOT NULL,
    cep VARCHAR(9)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

INSERT INTO estabelecimento (tipo_pessoa, nome_razao_social, nome_fantasia, cpf_cnpj, inscricao_estadual, telefone, email, logradouro, numero, bairro, cidade, estado, cep)
    VALUES('JURIDICA', 'JOSE COMERCIO DE ALIMENTOS LTDA', 'CANTINA DO JOSÉ', '35.884.537/0001-46', '458713321', '(99) 99999-9999', 'cantinadojose@gmail.com',
            'Rua A', 1354, 'Bairro A', 'Cidade A', 'AA', '99999-999');

INSERT INTO estabelecimento (tipo_pessoa, nome_razao_social, nome_fantasia, cpf_cnpj, inscricao_estadual, telefone, email, logradouro, numero, bairro, cidade, estado, cep)
    VALUES('JURIDICA', 'JOAO COMERCIO DE ALIMENTOS LTDA', 'CANTINA DO JOÃO', '39.348.813/0001-02', '2152652315', '(99) 99999-9999', 'cantinadojoao@gmail.com',
            'Rua B', 5487, 'Bairro B', 'Cidade A', 'BB', '99999-999');
INSERT INTO estabelecimento (tipo_pessoa, nome_razao_social, nome_fantasia, cpf_cnpj, inscricao_estadual, telefone, email, logradouro, numero, bairro, cidade, estado, cep)
    VALUES('JURIDICA', 'MARIA COMERCIO DE ALIMENTOS LTDA', 'CANTINA DA MARIA', '70.213.564/0001-98', '1241648754', '(99) 99999-9999', 'cantinadamaria@gmail.com',
            'Rua C', 5487, 'Bairro C', 'Cidade C', 'CC', '99999-999');

INSERT INTO estabelecimento (tipo_pessoa, nome_razao_social, nome_fantasia, cpf_cnpj, inscricao_estadual, telefone, email, logradouro, numero, bairro, cidade, estado, cep)
    VALUES('JURIDICA', 'FRANCISCA COMERCIO DE ALIMENTOS LTDA', 'CANTINA DA FRANCISCA', '65.285.453/0001-57', '5871336597', '(99) 99999-9999', 'cantinadafrancisca@gmail.com',
            'Rua D', 5487, 'Bairro D', 'Cidade D', 'DD', '99999-999');