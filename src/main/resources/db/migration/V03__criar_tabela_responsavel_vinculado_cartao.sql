CREATE TABLE responsavel (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL UNIQUE,
    telefone VARCHAR(16) UNIQUE,
    usuario_id BIGINT(20) NOT NULL,
    FOREIGN KEY (usuario_id) REFERENCES usuario(id)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

CREATE TABLE vinculado (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL,
    genero VARCHAR(1) NOT NULL,
    data_nascimento DATE NOT NULL,
    telefone VARCHAR(16),
    estabelecimento_id BIGINT(20) NOT NULL,
    responsavel_id BIGINT(20) NOT NULL,
    usuario_id BIGINT(20) NOT NULL,
    FOREIGN KEY (estabelecimento_id) REFERENCES estabelecimento(id),
    FOREIGN KEY (responsavel_id) REFERENCES responsavel(id),
    FOREIGN KEY (usuario_id) REFERENCES usuario(id)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

CREATE TABLE cartao (
    id BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    codigo VARCHAR(50) NOT NULL UNIQUE,
    ilimitado BOOLEAN DEFAULT 0,
    limite_diario DECIMAL(16, 2),
    ativo BOOLEAN DEFAULT 1,
    responsavel_id BIGINT(20),
    vinculado_id BIGINT(20),
    FOREIGN KEY (responsavel_id) REFERENCES responsavel(id),
    FOREIGN KEY (vinculado_id) REFERENCES vinculado(id)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;


INSERT INTO responsavel (nome, email, telefone, usuario_id) VALUES ('João da Silva', 'joao.silva@silva.com', '(69) 98996-2827', (SELECT id FROM usuario WHERE nome = 'João da Silva'));
INSERT INTO responsavel (nome, email, telefone, usuario_id) VALUES ('Maria da Costa', 'maria.costa@costa.com', '(69) 99766-5095', (SELECT id FROM usuario WHERE nome = 'Maria da Costa'));
INSERT INTO responsavel (nome, email, telefone, usuario_id) VALUES ('Joaquim Ribeiro', 'joaquim.rebeiro@ribeiro.com', '(69) 98396-8091', (SELECT id FROM usuario WHERE nome = 'Joaquim Ribeiro'));
INSERT INTO responsavel (nome, email, telefone, usuario_id) VALUES ('Astrogilda Maranhão', 'astrogilda.maranhao@maranhao.com', '(69) 99135-6293', (SELECT id FROM usuario WHERE nome = 'Astrogilda Maranhão'));

/* Vinculados do João da Silva */
INSERT INTO vinculado (nome, genero, data_nascimento, telefone, estabelecimento_id, responsavel_id, usuario_id) VALUES ('Paulo da Silva', 'M', '2012-05-13', '(69) 99321-4366',
    (SELECT id FROM estabelecimento WHERE cpf_cnpj = '35.884.537/0001-46'), 
    (SELECT id FROM responsavel WHERE email = 'joao.silva@silva.com'),
    (SELECT id FROM usuario WHERE nome = 'Paulo da Silva'));

INSERT INTO vinculado (nome, genero, data_nascimento, telefone, estabelecimento_id, responsavel_id, usuario_id) VALUES ('Pamela da Silva', 'F', '2013-06-05', '(69) 99362-9397',
    (SELECT id FROM estabelecimento WHERE cpf_cnpj = '35.884.537/0001-46'), 
    (SELECT id FROM responsavel WHERE email = 'joao.silva@silva.com'),
    (SELECT id FROM usuario WHERE nome = 'Pamela da Silva'));
/* end */

/* Vinculados da Maria da Costa*/
INSERT INTO vinculado (nome, genero, data_nascimento, telefone, estabelecimento_id, responsavel_id, usuario_id) VALUES ('Marlinda da Costa', 'F', '2012-02-20', '(69) 99696-4721',
    (SELECT id FROM estabelecimento WHERE cpf_cnpj = '39.348.813/0001-02'), 
    (SELECT id FROM responsavel WHERE email = 'maria.costa@costa.com'),
    (SELECT id FROM usuario WHERE nome = 'Marlinda da Costa'));

INSERT INTO vinculado (nome, genero, data_nascimento, telefone, estabelecimento_id, responsavel_id, usuario_id) VALUES ('Mario da Costa', 'M', '2013-08-25', '(69) 98338-6594',
    (SELECT id FROM estabelecimento WHERE cpf_cnpj = '39.348.813/0001-02'), 
    (SELECT id FROM responsavel WHERE email = 'maria.costa@costa.com'),
    (SELECT id FROM usuario WHERE nome = 'Mario da Costa'));
/* end */

/* Vinculados do Joaquim Ribeiro */
INSERT INTO vinculado (nome, genero, data_nascimento, telefone, estabelecimento_id, responsavel_id, usuario_id) VALUES ('Jairo Ribeiro', 'M', '2012-09-10', '(69) 99523-6890',
    (SELECT id FROM estabelecimento WHERE cpf_cnpj = '70.213.564/0001-98'), 
    (SELECT id FROM responsavel WHERE email = 'joaquim.rebeiro@ribeiro.com'),
    (SELECT id FROM usuario WHERE nome = 'Jairo Ribeiro'));

INSERT INTO vinculado (nome, genero, data_nascimento, telefone, estabelecimento_id, responsavel_id, usuario_id) VALUES ('Jaqueline Ribeiro', 'F', '2013-01-30', '(69) 99307-3651',
    (SELECT id FROM estabelecimento WHERE cpf_cnpj = '70.213.564/0001-98'), 
    (SELECT id FROM responsavel WHERE email = 'joaquim.rebeiro@ribeiro.com'),
    (SELECT id FROM usuario WHERE nome = 'Jaqueline Ribeiro'));
/* end */

/* Vinculados da Astrogilda Maranhão */
INSERT INTO vinculado (nome, genero, data_nascimento, telefone, estabelecimento_id, responsavel_id, usuario_id) VALUES ('Amador Maranhão', 'M', '2012-07-22', '(69) 98834-7348',
    (SELECT id FROM estabelecimento WHERE cpf_cnpj = '65.285.453/0001-57'), 
    (SELECT id FROM responsavel WHERE email = 'astrogilda.maranhao@maranhao.com'),
    (SELECT id FROM usuario WHERE nome = 'Amador Maranhão'));

INSERT INTO vinculado (nome, genero, data_nascimento, telefone, estabelecimento_id, responsavel_id, usuario_id) VALUES ('Amanda Maranhão', 'F', '2013-04-29', '(69) 99138-5685',
    (SELECT id FROM estabelecimento WHERE cpf_cnpj = '65.285.453/0001-57'), 
    (SELECT id FROM responsavel WHERE email = 'astrogilda.maranhao@maranhao.com'),
    (SELECT id FROM usuario WHERE nome = 'Amanda Maranhão'));
/* end */

INSERT cartao(codigo, ilimitado, responsavel_id) VALUES ('6526B6AA5B6C', 1, (select id from responsavel where email = 'joao.silva@silva.com'));
INSERT cartao(codigo, ilimitado, limite_diario, vinculado_id) VALUES ('8952D714FE15', 0, 10.0, (select id from vinculado where nome = 'Paulo da Silva'));
INSERT cartao(codigo, ilimitado, limite_diario, vinculado_id) VALUES ('8246BD5B11B3', 0, 15.0, (select id from vinculado where nome = 'Pamela da Silva'));

INSERT cartao(codigo, ilimitado, responsavel_id) VALUES ('1975088A36E3', 1, (select id from responsavel where email = 'maria.costa@costa.com'));
INSERT cartao(codigo, ilimitado, limite_diario, vinculado_id) VALUES ('88CB7C0D5372', 0, 12.0, (select id from vinculado where nome = 'Marlinda da Costa'));
INSERT cartao(codigo, ilimitado, limite_diario, vinculado_id) VALUES ('2D517C85D2EB', 0, 10.0, (select id from vinculado where nome = 'Mario da Costa'));

INSERT cartao(codigo, ilimitado, responsavel_id) VALUES ('5DBC375D0565', 1, (select id from responsavel where email = 'joaquim.rebeiro@ribeiro.com'));
INSERT cartao(codigo, ilimitado, limite_diario, vinculado_id) VALUES ('03597B8B7BBE', 0, 15.0, (select id from vinculado where nome = 'Jairo Ribeiro'));
INSERT cartao(codigo, ilimitado, limite_diario, vinculado_id) VALUES ('202CC7A3A0CE', 0, 12.0, (select id from vinculado where nome = 'Jaqueline Ribeiro'));

INSERT cartao(codigo, ilimitado, responsavel_id) VALUES ('0F599858EABF', 1, (select id from responsavel where email = 'astrogilda.maranhao@maranhao.com'));
INSERT cartao(codigo, ilimitado, limite_diario, vinculado_id) VALUES ('C6A03C497FB4', 0, 10.0, (select id from vinculado where nome = 'Amador Maranhão'));
INSERT cartao(codigo, ilimitado, limite_diario, vinculado_id) VALUES ('7A1960D00422', 0, 15.0, (select id from vinculado where nome = 'Amanda Maranhão'));